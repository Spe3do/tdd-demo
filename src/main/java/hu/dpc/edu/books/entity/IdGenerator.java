package hu.dpc.edu.books.entity;

public interface IdGenerator {
    long generateId();
}
