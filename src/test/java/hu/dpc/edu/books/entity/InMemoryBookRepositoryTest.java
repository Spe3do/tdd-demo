package hu.dpc.edu.books.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hu.dpc.edu.books.entity.TestUtil.assertBookEquals;
import static hu.dpc.edu.books.entity.TestUtil.bookEqualsTo;
import static hu.dpc.edu.books.entity.TestUtil.notSame;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class InMemoryBookRepositoryTest {

    private InMemoryBookRepository repository;
    private IdGenerator idGenerator;
    private Map<Long, Book> bookById;
    private Map<Long, Book> realBookByIdMap;
    private Book book;

    @BeforeEach
    void beforeEach() {
        realBookByIdMap = new HashMap<>();
        bookById = spy(realBookByIdMap);
        idGenerator = mock(IdGenerator.class);
        repository = new InMemoryBookRepository(bookById, idGenerator);
        book = mock(Book.class);
    }

//    @Test
//    void valami() {
//        final Book b1 = new Book();
//        bookById.put(123L, b1);
//        final Book b2 = bookById.get(123L);
//        assertSame(b1, b2);
//    }

    @Test
    @DisplayName("add should throw IllegalArgumentException if book.title is null")
    void bookTitleIsRequired() {

        given(book.getAuthor())
                .willReturn("Anonymous");

        given(book.getTitle())
                .willReturn(null);

        final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            repository.add(book);
        });

        assertEquals("Book title is required", ex.getMessage());
    }

    @Test
    @DisplayName("add should throw NullPointerException if provided book is null")
    void shouldThrowNPEIfBookNull() {
        final NullPointerException npe = assertThrows(NullPointerException.class, () -> {
            repository.add(null);
        });

        assertEquals("book should not be null", npe.getMessage());
    }

    @Nested
    @DisplayName("add with a valid book")
    class AddWithProperBook {

        private long generatedId;

        private Book expectedBookToPut;

        @BeforeEach
        void beforeEach() {

            generatedId = 42;

            given(idGenerator.generateId())
                    .willReturn(generatedId);

            given(book.getAuthor())
                    .willReturn("Douglas Adams");
            given(book.getTitle())
                    .willReturn("The Hitchhikers Guide to the Galaxy");

            expectedBookToPut = new Book( generatedId,
                    "The Hitchhikers Guide to the Galaxy",
                    "Douglas Adams");
        }

        @Test
        @DisplayName("idGenerator.generateId() should be called")
        void addShouldCallGenerateId() {
            repository.add(book);

            verify(idGenerator).generateId();
        }

        @Test
        @DisplayName("bookById.put should be called with (generated id, book)")
        void addShouldCallPut() {
            repository.add(book);

            verify(bookById).put(generatedId, expectedBookToPut);
        }

        @Test
        @DisplayName("bookById.put should be called with a copy of the passed-in book")
        void addShouldCallPutWithNotTheSameReference() {
            repository.add(book);

            verify(bookById).put(eq(generatedId), argThat(notSame(book)));
        }


        @Test
        @DisplayName("should return with the generated id")
        void addShouldReturnGeneratedId() {
            final long result = repository.add(book);

            assertEquals(generatedId, result);
        }

    }

    @Nested
    @DisplayName("findById")
    class FindById {

        private Book returnedBook;
        private long searchedId;

        @BeforeEach
        void beforeEach() {
            searchedId = 42;
            returnedBook = mock(Book.class);
        }

        @Test
        @DisplayName("should throw EntityNotFoundException if bookById map does not contain the given id")
        void entityNotFound() {

            given(bookById.containsKey(searchedId))
                    .willReturn(false);
            given(bookById.get(searchedId))
                    .willReturn(null);


            assertThrows(EntityNotFoundException.class, () -> {
                repository.findById(searchedId);
            });

            int failures = 0;
            Error err = null;
            try {
                verify(bookById).containsKey(searchedId);
            } catch (Error error) {
                failures++;
                err = error;
            }
            try {
                verify(bookById).get(searchedId);
            } catch (Error error) {
                failures++;
                err = error;
            }

            assertNotEquals(2, failures, "At least map.get or map.containsKey should be called");
        }


        @Test
        @DisplayName("should return with bookById.get(id)")
        void shouldReturnMapGet() {
            given(bookById.containsKey(searchedId))
                    .willReturn(true);
            given(bookById.get(searchedId))
                    .willReturn(returnedBook);

            final Book result = repository.findById(searchedId);

            assertBookEquals(returnedBook,result);

        }


    }

    @Nested
    @DisplayName("findAll")
    class FindAll {


        private Book book1;
        private Book book2;
        private Book book3;
        private long book1Id = 42;
        private long book2Id = 48;
        private long book3Id = 68;

        @BeforeEach
        void beforeEach() {

            book1 = new Book(12L, "T1","A1");
            book2 = new Book(13L, "T2","A2");
            book3 = new Book(14L, "T3","A3");
            realBookByIdMap.put(book1Id, book1);
            realBookByIdMap.put(book2Id, book2);
            realBookByIdMap.put(book3Id, book3);
            repository = new InMemoryBookRepository(realBookByIdMap, idGenerator);
        }

        @Test
        @DisplayName("should return all values of bookById map")
        public void valuesOfBookById() {
            final List<Book> result = repository.findAll();

            assertTrue(result.contains(book1), "book1 is not in the list");
            assertTrue(result.contains(book2), "book2 is not in the list");
            assertTrue(result.contains(book3), "book3 is not in the list");
        }

        @Test
        @DisplayName("should return copies instead of the internal references")
        public void notTheOriginalReferences() {
            final List<Book> result = repository.findAll();

            assertTrue(result.stream()
                    .filter(book -> book == book1)
                    .count() == 0, "book1 reference leaked");
            assertTrue(result.stream()
                    .filter(book -> book == book2)
                    .count() == 0, "book2 reference leaked");
            assertTrue(result.stream()
                    .filter(book -> book == book3)
                    .count() == 0, "book3 reference leaked");
        }
    }

    @Nested
    @DisplayName("update")
    class UpdateRepository {

        private Book managedBook1;
        private Book managedBook2;
        private Book updatedBook;
        private long managedBookID1 = 42;
        private long managedBookID2 = 32;

        @BeforeEach
        void beforeEach() {
            managedBook1 = mock(Book.class);
            managedBook2 = mock(Book.class);
            updatedBook = mock(Book.class);
            realBookByIdMap.put(managedBookID1, managedBook1);
            realBookByIdMap.put(managedBookID2, managedBook2);
            repository = new InMemoryBookRepository(realBookByIdMap, idGenerator);
        }

        @Test
        @DisplayName("should throw a NPE if the input is null")
        public void throwNPEIfBookNull() {
            final NullPointerException npe = assertThrows(NullPointerException.class, () -> {
                repository.update(null);
            });

            assertEquals("book should not be null", npe.getMessage());

        }

        @Test
        @DisplayName("should throw EntityNotFound exception if given book is not found in the repository")
        public void throwEntityNotFoundException() {
            given(updatedBook.getId()).willReturn(33L);
            given(updatedBook.getAuthor()).willReturn("Author");
            given(updatedBook.getTitle()).willReturn("Title");

            assertThrows(EntityNotFoundException.class, () -> {
                repository.update(updatedBook);
            });
        }

        @Test
        @DisplayName("should throw IllegalArgumentException if book's title is null")
        public void throwIllegalArgumentExceptionIfTitleNull() {
            given(updatedBook.getTitle()).willReturn(null);

            assertThrows(IllegalArgumentException.class, () -> {
                repository.update(updatedBook);
            });
        }

        @Test
        @DisplayName("should throw IllegalArgumentException if book's title is null or empty")
        public void throwIllegalArgumentExceptionIfTitleIsEmpty() {
            given(updatedBook.getTitle()).willReturn("");

            assertThrows(IllegalArgumentException.class, () -> {
                repository.update(updatedBook);
            });
        }

        @Test
        @DisplayName("should throw IllegalArgumentException if book's Author is null")
        public void throwIllegalArgumentExceptionIfAuthorIsNull() {
            given(updatedBook.getTitle()).willReturn("Hobbit");
            given(updatedBook.getAuthor()).willReturn(null);

            assertThrows(IllegalArgumentException.class, () -> {
                repository.update(updatedBook);
            });
        }

        @Test
        @DisplayName("should throw IllegalArgumentException if book's Author is empty")
        public void throwIllegalArgumentExceptionIfAuthorIsEmpty() {
            given(updatedBook.getTitle()).willReturn("Hobbit");
            given(updatedBook.getAuthor()).willReturn("");

            assertThrows(IllegalArgumentException.class, () -> {
                repository.update(updatedBook);
            });
        }

        @Test
        @DisplayName("should modify the right entry in the map")
        public void shouldModifyTheRightEntry() {
            given(updatedBook.getTitle()).willReturn("Hobbit");
            given(updatedBook.getAuthor()).willReturn("J.R.R. Tolkien");
            given(updatedBook.getId()).willReturn(managedBookID1);

            repository.update(updatedBook);

            verify(managedBook1).setAuthor("J.R.R. Tolkien");
            verify(managedBook1).setTitle("Hobbit");

        }
    }


}