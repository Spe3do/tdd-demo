package hu.dpc.edu.books.entity;

import org.junit.jupiter.api.Test;

import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTest {

    @Test
    public void testForEach() {
        final List<Book> books = List.of(
                new Book("Zabhegyezo", "J. D. Salinger"),
                new Book("Galaxis...", "Douglas Adams"),
                new Book("Alapitvany", "Asimov"));

        PrintStream ps = System.out;

        //books.forEach(book -> System.out.println(book));
        //books.forEach(ps::println);
        books.forEach(System.out::println);


    }
    @Test
    public void testStreams() {


        final Stream<Book> bookStream = Stream.of(
                new Book("Zabhegyezo", "J. D. Salinger"),
                new Book("Galaxis", "Douglas Adams"),
                new Book("Alapitvany", "Asimov"));


        final IntStream filteredTitleStream = bookStream
                .map(Book::getTitle)
                .mapToInt(String::length)
                .filter(length -> length < 10);

        final long count = filteredTitleStream.count();

        final List<Book> books = List.of(
                new Book("Zabhegyezo", "J. D. Salinger"),
                new Book("Galaxis...", "Douglas Adams"),
                new Book("Alapitvany", "Asimov"));

        final Stream<Book> stream = books.stream();

        final Stream<String> titleStream = stream
                .map(book -> book.getTitle());

        final Stream<String> upperCaseTitleStream = titleStream
                .map(String::toUpperCase);

        final List<String> collected = upperCaseTitleStream
                .collect(Collectors.toList());

        System.out.println(collected);


    }
}
